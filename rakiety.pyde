add_library('controlP5')

lifespan = 300
count = 0
target = PVector(700,400)
generation= 1
fittab = []
afittab=[]
mutate = True
stopping = True
global lifeslider
class Start:
    pos=PVector(100,400)
    r=16
    dragged=False
    def show(self):
        fill(0,0,255,90)
        ellipse(self.pos.x,self.pos.y,self.r,self.r)
        if(dist(self.pos.x,self.pos.y,mouseX,mouseY)<16):
            text("start",self.pos.x-25,self.pos.y-15)
        if(self.dragged):
            self.pos.x=mouseX
            self.pos.y=mouseY
    def checkdrag(self):
        if(dist(self.pos.x,self.pos.y,mouseX,mouseY)<16 and mousePressed):
            self.dragged=True
            print(self.dragged)
        else:
            self.dragged=False

s = Start()

class DNA:
    
    def __init__(self,genes):
        if(len(genes)>1):
            self.genes=genes
        else:
            self.genes = []
            for i in range(lifespan):
                x = PVector.random2D()
                x.setMag(0.1)
                self.genes.append(x)

    def  crossover(self,parent):
        self.newdna=[]
        mid = floor(random(len(self.genes)))
        for i in range(len(self.genes)):
            if (i>mid):
                self.newdna.append(self.genes[i])
            else:
                self.newdna.append(parent.genes[i])
        return DNA(self.newdna)
            
    def mutation(self):
        for i in range(len(self.genes)):
            if(random(1)<mutslider.getValue()):
                self.genes[i]=PVector.random2D()
                self.genes[i].setMag(0.1)
class Rocket:
    pos = PVector(s.pos.x,s.pos.y)
    vel = PVector(0,0)
    acc = PVector()
    
    def __init__(self,dna):
        if(dna):
            self.dna=dna
        else:
            self.dna = DNA([])
    
        self.complete = False
        self.pos = PVector(s.pos.x,s.pos.y)
        self.vel = PVector()
        self.acc = PVector()
    def applyForce(self,force):
        self.acc.add(force)
    def update(self):
        self.d = dist(self.pos.x,self.pos.y,t.pos.x,t.pos.y)
        if(not self.complete and self.d<=targetslider.getValue()):
            pass
            if(stopping):
                self.complete = True
                self.dna.genes[count]= self.vel.copy().mult(-1)
            #self.pos=t.pos.copy()
        if(not self.complete):
            self.applyForce(self.dna.genes[count])
            self.vel.add(self.acc)
            self.pos.add(self.vel)
            self.acc.mult(0)
        else:
            pass
            self.dna.genes[count]= self.vel.copy().mult(0)
        
           
        
    def show(self):
        point(self.pos.x,self.pos.y)
        pushMatrix()
        translate(self.pos.x,self.pos.y)
        rotate(self.vel.heading());
        fill(0,0,50,10)
        rectMode(CENTER)
        rect(0,0,20,10)
        triangle(10,5,10,-5,20,0)
        
        popMatrix()
    def calcFitness(self):
        if(self.complete):
            self.fitness=1/14.0
        else:
            self.d = dist(self.pos.x,self.pos.y,t.pos.x,t.pos.y)
            self.fitness=1/self.d
   

class Population:
    rockets = []
    popsize = 25;
    matingpool=[]
    maxfit=0;
    for i in range(popsize):
        x= Rocket([])
        rockets.append(x)

    def run(self):
        for i in range(len(self.rockets)):
            self.rockets[i].update()
            self.rockets[i].show()
    def evaluate(self):
        self.maxfit=0;
        self.allfit=0
        for r in self.rockets:
            r.calcFitness()
            if(r.fitness>self.maxfit):
                self.maxfit=r.fitness
        self.matingpool=[]
        for r in self.rockets:
            self.allfit+=r.fitness
            r.fitness/=self.maxfit
        
            
        for r in self.rockets:
            
            n = r.fitness*100
            for j in range(floor(n)):
                self.matingpool.append(r)
        self.allfit/=25
    def selection(self):
        newrock = []
        for i in range(len(self.rockets)):
            self.parentA = self.matingpool[floor(random(len(self.matingpool)))].dna
            self.parentB = self.matingpool[floor(random(len(self.matingpool)))].dna
            self.child = self.parentA.crossover(self.parentB)
            if(mutate):
                self.child.mutation()
                print("mutating")
            newrock.append(Rocket(self.child))
        self.rockets = newrock
        
        

            
class Target:
    pos=PVector(600,400)
    r=16
    dragged=False
    def show(self):
        fill(255,0,0,90)
        ellipse(self.pos.x,self.pos.y,targetslider.getValue(),targetslider.getValue())
        if(dist(self.pos.x,self.pos.y,mouseX,mouseY)<targetslider.getValue()):
            text("target",self.pos.x-25,self.pos.y-15)
        if(self.dragged):
            self.pos.x=mouseX
            self.pos.y=mouseY
    def checkdrag(self):
        if(dist(self.pos.x,self.pos.y,mouseX,mouseY)<targetslider.getValue() and mousePressed):
            self.dragged=True
            print(self.dragged)
        else:
            self.dragged=False
            
pop = Population()
t= Target()
s = Start()

def setup():
    cp5 = ControlP5(this)
    global mutbutton
    global mutslider
    global stopbutton
    global targetslider
    global lifeslider
    mutbutton = cp5.addButton("Mutation").setValue(0).setPosition(905,223).setSize(100,19).addListener(colorA).setColorBackground(color(104, 255, 142) ).setColorLabel(color(0)).setColorActive(color(247, 247, 247)).setColorForeground(color(247, 247, 247))  
    mutslider = cp5.addSlider("Chance").setPosition(1030,223).setSize(100,19).setRange(0.01,0.1).setNumberOfTickMarks(100).setSliderMode(Slider.FLEXIBLE).setColorBackground(color( 180,180,180,90 ) ).setColorLabel(color(0)).setColorForeground(color(104, 255, 142) ).setColorValueLabel(color(0)).setColorActive(color(247, 247, 247))  
    stopbutton = cp5.addButton("Stop on hitting target").setValue(0).setPosition(905,290).setSize(100,19).addListener(stopf).setColorBackground(color(104, 255, 142) ).setColorLabel(color(0)).setColorActive(color(247, 247, 247)).setColorForeground(color(247, 247, 247)) 
    targetslider = cp5.addSlider("Target Size").setPosition(905,317).setSize(100,20).setRange(0,100).setNumberOfTickMarks(11).setColorBackground(color( 180,180,180,90 ) ).setColorLabel(color(0)).setColorForeground(color(242, 96, 96) ).setColorValueLabel(color(0)).setColorActive(color(247, 247, 247)).setValue(15)
    lifeslider = cp5.addSlider("lifespan").setPosition(905,347).setSize(100,20).setRange(100,1000).setNumberOfTickMarks(11).setColorBackground(color( 180,180,180,90 ) ).setColorLabel(color(0)).setColorForeground(color(104, 255, 142) ).setColorValueLabel(color(0)).setColorActive(color(247, 247, 247)).setValue(15).addListener(lifef)
     
     
    count = 0
    size(1200,800)
    print(1/16.00)
def draw():
    global pop
    global mutslider
    global count
    global generation
    fill(255,90)
    rect(0,0,width,height)
    t.show()
    t.checkdrag()
    s.show()
    s.checkdrag()
    pop.run()
    count+=1
    if(count==lifespan):
        pop.evaluate()
        pop.selection()
        generation+=1
        count=0;
        if(len(fittab)>48):
            for i in range(len(fittab)-1):
                fittab[i]=fittab[i+1]
            fittab[48]=pop.maxfit
        else:
            fittab.append(pop.maxfit)
        if(len(afittab)>48):
            for i in range(len(afittab)-1):
                afittab[i]=afittab[i+1]
            afittab[48]=pop.allfit
        else:
            afittab.append(pop.allfit)
        
        print(pop.maxfit,pop.allfit)
    fill(240,0,0,50)

    fill(0,10)
    rect(100,0,330,25)
    textSize(12)
    fill(0,95)
    text(count,10,10)
    text("max fitness: "+str(round(pop.maxfit,4)),50,10)
    text("generation: "+str(generation),180,10)
    
    #LEFT MENU
    pushMatrix()
    translate(-10,0)
    rectMode(CORNER)
    fill(0,10)
    rect(900,15,300,175)
    
    rect(900,210,300,45)
    rect(900,275,300,105)
    fill(240,240,240)
    
    #NAME BOXES
    
    rect(1000,5,100,17)
    rect(1000,202,100,17)
    rect(1000,267,100,17)
    fill(0)

    textSize(13)
    textMode(CENTER)
    text("Mutation",1025,215)
    text("Fitness",1025,18)
    text("Options",1025,280)
    fill(0,10)
    rect(925,25,250,150)
    fill(255,0,0,80)
    for i in range(len(fittab)):
        rect(925+i*5,175,5,map(round(fittab[i],4),0,(1/14.00),0,-150))

    fill(0,255,0,80)
    for i in range(len(afittab)):
        
        rect(925+i*5,175,5,map(afittab[i],0,1/14.00,0,-150))
    fill(0,10)
    fill(255,0,0,80)
    rect(925,177,7,7)
    fill(0)
    textSize(10)
    text("max",935,184)
    
    fill(0,225,0,80)
    rect(960,177,7,7)
    fill(0)
    textSize(10)
    text("average",970,184)
    
    text(targetslider.getValue(),200,200)

    popMatrix()
    fill(30,10)
    ellipse(t.pos.x,t.pos.y,targetslider.getValue(),targetslider.getValue()-10)
def colorA(self):
    global mutbutton
    global mutate 
    global mutslider

    mutate= not mutate
    if(mutate):
        
        
        mutbutton.setColorBackground(color(104, 255, 142) )
        mutslider.setColorForeground(color(104, 255, 142)).setLock(False)
    else:
        mutbutton.setColorBackground(color( 150,150,150,90 ) )
        mutslider.setColorForeground(color(150,150,150)).setLock(True)
    print("xd")
    

def stopf(self):
    global stopping
    stopping = not stopping
    if(stopping):
        stopbutton.setColorBackground(color(104, 255, 142) )
    else:
        stopbutton.setColorBackground(color(150,150,150,90) )
    pass
    
def lifef(self):
    pass